//
//  MainViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 5/10/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RealmSwift

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    //Delegate: se encarga de hacer el scroll en la tabla.
    
    @IBOutlet weak var pokedexTableView: UITableView!
    var nextURL = "https://pokeapi.co/api/v2/pokemon"
    var pokemon = [Pokemon]()
    var searchController: UISearchController!
    
    var filteredPokemon = [Pokemon]()
    var isFiltering = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemon()
        let realm = try! Realm()
        print(realm.configuration.fileURL)
        
        configSearchBar()
        /* para reutilizar la celda en view controllers diferentes
        pokedexTableView.register(GeneralPokemonTableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
         */
    }
    
    func getPokemon(){
        Alamofire
            .request(URL(string: nextURL)!) //Siguiente que se va a consultar
            .responseObject{
                (response: DataResponse<PokemonResponse>) in
                self.pokemon += response.value?.pokemon ?? []
                self.pokedexTableView.reloadData()
                self.nextURL = response.value?.nextURL ?? "https://pokeapi.co/api/v2/pokemon"
        }
    }
    
    /*** Table View ***/
    //Numero de secciones de la tabla
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //Numero de filas por cada seccion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredPokemon.count : pokemon.count
        //Si se quiere diferentes filas en cada seccion hay que hacer un switch
    }
    //Informacion de celdas
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell //Del table view instancia la celda con ese identificador y le hace cast a PokemonTableViewCell
        
        var currentPokemon: Pokemon!
        if isFiltering {
            currentPokemon = filteredPokemon[indexPath.row]
        }else{
            currentPokemon = pokemon[indexPath.row]
        }
        
        //cell.textLabel?.text = "\(indexPath)" //El indexPath tiene el numero de la seccion y el numero de fila. Por ejemplo: [1, 1] Seccion 1 fila 1
        //cell.textLabel?.text = pokemon[indexPath.row].name?.capitalized ?? "n/a"
        cell.pokemonNameLabel?.text = currentPokemon.name?.capitalized ?? "n/a"
        let pokemonId = (pokemon.firstIndex{$0.name == currentPokemon.name} ?? 0) + 1
        cell.pokemonImageView
            .kf
            .setImage(with: URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(pokemonId).png"))
        return cell //Informacion que va a tener cada fila
    }
    
    //Ejecuta la transicion
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonSegue", sender: self)
    }
    
    //Infinite scroll. Mientras se hace scroll se van descargando lo que el usuario quiera ver
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Cuando se presenta el pokemon numero 20, manda a descargar los siguientes 20.
        if indexPath.row == pokemon.count - 1 {
            getPokemon()
        }
    }
    //MARK :- SEARCH BAR
    
    func configSearchBar(){
        //searchController: ayuda a controlar el flujo de la busqueda
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false //se hace oscuro la parte de la tabla mientra va a buscar
        searchController.searchResultsUpdater = self //clase que controla la tabla que tiene los resultados de la busqueda. Como se va a usar la misma tabla se pone self
        searchController.searchBar.delegate = self //cuando se escribe en la barra de busqueda, donde van a cambiar los resultados.
        searchController.searchBar.sizeToFit() //para que ocupe todo el ancho de la tabla
        
        pokedexTableView.tableHeaderView = searchController.searchBar //agrega la barra de busqueda al table view
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        //Al arreglo de pokemon le filtra por lo que vaya escribiendo en la barra de busqueda.
        filteredPokemon = pokemon.filter {
            ($0.name ?? "") //le quita la opcionalidad
                .lowercased()
                .contains(
                    (searchController.searchBar.text ?? "")
                        .lowercased()
            )}
        
        isFiltering = searchController.searchBar.text != ""
        pokedexTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //
        
    }
    
    //Se va a preparar para la transicion
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            
            var selectedPokemon : Pokemon!
            if isFiltering{
                selectedPokemon =  filteredPokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0] //El pokemon que se hizo tab en la celda
            }else{
                selectedPokemon =  pokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0] //El pokemon que se hizo tab en la celda
            }
            
            searchController.dismiss(animated: true){
                self.searchController.searchBar.text = ""
            }
            destination.pokemonURL = selectedPokemon.url!
            destination.pokemonName = selectedPokemon.name
            destination.isFavouriteScreen = false
        }
    }
}
