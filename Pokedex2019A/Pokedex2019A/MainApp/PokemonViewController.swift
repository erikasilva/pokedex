//
//  PokemonViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 6/4/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RealmSwift

class PokemonViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var typesTableView: UITableView!
    
    var pokemonName: String?
    var pokemonURL: String = ""
    var types = [PokemonType]() //Arreglo de tipo PokemonType
    var pokemonId: Int!
    var pokemon: Pokemon?
    var isFavouriteScreen: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pokemonName?.capitalized ?? "Pokemon"
        downloadPokemonInfo()
        //Inicializa un button con el titulo Add. favTapped es la accion del boton. No se utiliza el IBAction sino una especie de observador.
        //#selector --> pasa por el compilador de Objective C.
        let button: UIBarButtonItem?
        if(!isFavouriteScreen){
            button = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(favTapped))
        }else{
            button = UIBarButtonItem(title: "Remove", style: .done, target: self, action: #selector(removeFavourite))
        }
        
        self.navigationItem.rightBarButtonItem = button
    }
    //Funcion compilable en objective C.
    @objc func favTapped(){
        //UserDefaults: memoria del telefono, permite guardar primitivos o arreglos de primitivos, string.
        let userDefaults = UserDefaults.standard
        var favouritePokemon = userDefaults.array(forKey: "favPokemon") ?? [] // ?? []  si no existe ninguno crea un array vacio
        favouritePokemon.append(pokemonId)
        userDefaults.set(favouritePokemon, forKey: "favPokemon")
        let realm = try! Realm()
        try! realm.write {
            let pokemonEntity = PokemonEntity(pokemon: pokemon!)
            pokemonEntity.url = pokemonURL
            pokemonEntity.isFavourite = true
            realm.add(pokemonEntity, update: true)
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func removeFavourite(){
        let realm = try! Realm()
        try! realm.write {
            let pokemonEntity = PokemonEntity(pokemon: pokemon!)
            pokemonEntity.url = pokemonURL
            pokemonEntity.isFavourite = false
            realm.add(pokemonEntity, update: true)
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
    func downloadPokemonInfo(){
        Alamofire.request(pokemonURL).responseObject { (response: DataResponse<Pokemon>) in
            self.weightLabel.text = "\(response.value?.weight ?? 0)"
            self.heightLabel.text = "\(response.value?.height ?? 0)"
            self.types = response.value?.types ?? []
            self.pokemonId = response.value?.pokemonId
            self.typesTableView.reloadData()
            self.pokemonImageView.kf.setImage(with: URL(string: response.value?.imageURL ?? "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/0.png"))
            self.pokemon = response.value
        }
    }
    //UITableViewDataSoure
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = types[indexPath.row].name ?? "n/a"
        return cell
    }
}
