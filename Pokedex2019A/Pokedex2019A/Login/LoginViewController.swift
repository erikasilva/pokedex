//
//  LoginViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 4/16/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class LoginViewController: UIViewController {
    //Es la manera en la que se maneja la memoria de los elementos.
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var pokedexImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    /*
        pokedexImageView.image = UIImage(named: "pokedex") -- Para agregar imagenes por codigo, named: es el nombre del image set donde se agrega la imagen.
        pokedexImageView.contentMode = .scaleAspectFit
    */
        activityIndicator.hidesWhenStopped = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        //La diferencia con el if es que lo que se crea en el guard se puede leer afuera del bloque.
        //Si es que no hay un usuario loggeado se va a quedar en la pantalla de login.
    
        guard let currentUser = Auth.auth().currentUser else{
            return
        }
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        //Si es que ya esta registrado se va al Main View Controller sino se va a la pantalla de Registro
        let userRef = db.collection("users").document(currentUser.uid)
        userRef.getDocument{(snapshot, error) in
            self.activityIndicator.stopAnimating()
            if error != nil || snapshot?.get("registerCompleted") == nil{ //Si es que no hay un usuario registrado, se va a la pantalla de registro
                self.performSegue(withIdentifier: "toRegisterSegue", sender: self)
            }else{
                self.performSegue(withIdentifier: "toMainSegue", sender: self)
            }
        }
    }
    
    //Para que el teclado aparezca solo en los textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any, forEvent event: UIEvent) {
        /*let mail = "erika@mail.com"
        let password = "1234"
        if (mail, password) == (emailTextField.text, passwordTextField.text) {
            performSegue(withIdentifier: "toMainView", sender: self) //La aplicacion sabe que debe ejecutar la transicion toMainView.
            return
        }
        showAlertError()
        */
        
        firebaseAuth(email: emailTextField.text!, password: passwordTextField.text!)
    }
    /*
     Mensajes en la pantalla:
     Dos formas:
     1) Alert:
     2)
     */
    func firebaseAuth(email: String, password: String){
        Auth.auth().signIn(withEmail: email, password: password){(result, error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.performSegue(withIdentifier: "toMainView", sender: self)
        }
    }
    
    func showAlertError(){
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials", preferredStyle: .alert)
        
        //En UIAlertAction: en el 3er parametro recibe una funcion.
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            //unowned self: Si se tiene referencias que son strong, no se puede utilizar ya que crea un aumento de referencias en memoria. Con referencias weak no pasa nada.
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        present(alertView, animated: true, completion: nil) //Parametros: 1. Lo que se quiera presentar en este caso la alerta de arriba, 2. Si se tiene alguna animacion, 3. Apenas termina de aparecer el alert se ejecuta el codigo que se ponga en completion.
        alertView.addAction(okAlertAction)
    }
    /*
    //Higher-ordered functions: reciben como parametros o retornan funciones.
    func handleOperation(n1: Int, n2: Int, operation: (Int, Int) -> (Int)){
        print(operation(n1, n2))
    }
    func operation(_ on1: Int,_ on2: Int) -> Int{
        return on1 + on2
    }
    USO:
     handleOperation(n1: 4, n2: 5, operation: operation(_:_:))
     handleOperation(n1: 6, n2: 8) { (_, _) -> Int in
     <#code#>
     }
     */
    
    @IBAction func createAccountButtonPressed(_ sender: Any, forEvent event: UIEvent) {
            
    }
}
