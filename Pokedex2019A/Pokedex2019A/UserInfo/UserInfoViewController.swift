//
//  UserInfoViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 5/10/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
//Extension es una extension. En swift se puede extender todo. Por ejemplo se crea una extension para un boton que diga pintar de azul entonces todos los botones cumpliran esa funcion.
//Una buena práctica es tener extension de UIColor.
extension Date{
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMM yyyy"
        return formatter
    }()
    var formatted: String{
        return Date.formatter.string(from: self)
    }
}

class UserInfoViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nickTextField: UITextField!
    @IBOutlet weak var birthDayTextField: UITextField!

    let datePickerView = UIDatePicker()
    let imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        //Cada vez que se cambie el valor del datePicker, se ejecuta la funcion handleDatePicker
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        imagePickerController.delegate = self
    }
    //@objc --> es como un observador que esta esperando que algo pase.
    //Esto viene del compilador de objective c.
    @objc func handleDatePicker(_ datePicker: UIDatePicker){
        birthDayTextField.text = datePickerView.date.formatted
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.cornerRadius = userImageView.frame.height / 2.0
        userImageView.layer.masksToBounds = true
        birthDayTextField.inputView = datePickerView
    }
    
    @IBAction func addPictureButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default){ (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default){ (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        //Acciones para el Alert Controller
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else {
            print("Error Picking Image")
            return
        }
        self.userImageView.image = image
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let userId = String(Auth.auth().currentUser!.uid)
        let storage = Storage.storage()
        let usersImage = storage.reference().child("users")
        let currentUserImageRef = usersImage.child("\(userId).jpg")
        
        let userImage = userImageView.image
        let data = userImage?.jpegData(compressionQuality: 1)
        let uploadTask = currentUserImageRef.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            let size = metadata.size
            currentUserImageRef.downloadURL{ (url, error) in
                guard let downloadURL = url else {
                    return
                }
                
            }
        }
        
        let db = Firestore.firestore() //Se crea una referencia a Firestore
    db.collection("users").document(userId).setData([ //Cada usuario tiene un uid propio.
            "name": nameTextField.text ?? "",
            "nick": nickTextField.text ?? "",
            "birthday": datePickerView.date,
            "registerCompleted": true
        ]){
            (error) in
            print(error ?? "No error")
        }
    }
}

/*
 Constraints Images
 1. Ctrl-Click encima de la imagen: Aspect Radio.
 2. Ctrl-Click en el View Controller: Multiplier 1:4. Relacion 1/4 con el View.
 */

/*
 VIEW CONTROLLER LIFECYCLE:
 1. Es ejecutado por un click ... ejecuta la funcion loadView.
 2. Ejecuta el metodo viewDidLoad
 3. Ejecuta el metodo viewWillAppear -- se aplican los contraints, tamaños, etc
 4. Ejecuta el metodo viewDidAppear -- se muestra todo en pantalla ya se tiene toda la funcionalidad en pantalla.
 5. Cuando ya desaparece la pantalla, se ejecutan los metodos viewWillDisappear y viewDidDisappear.
 */
/*
 Did:

 Will:
*/
