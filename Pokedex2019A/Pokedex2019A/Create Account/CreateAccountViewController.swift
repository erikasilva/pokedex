//
//  CreateAccountViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 4/30/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseAuth

class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmPasswordTextField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true) //Para que se cierre el teclado cuando se termine de editar.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if passwordTextField.text != confirmPasswordTextField.text {
            showAlertError(withMessage: "Passwords don't match")
        }
    }
    
    func showAlertError(withMessage message: String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!){
            (result, error) in
            if let _ = error {
                self.showAlertError(withMessage: error?.localizedDescription ?? "Error")
            }else{ //Si es que la creacion es exitosa, se ira a la pantalla Main
                let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController //Se instancia como un storyboard. "MainViewController" es el ID del storyboard.
                self.present(mainViewController, animated: true, completion: nil)
            }
        }
    }
    @IBAction func cancelButtonPressed(_ sender: Any, forEvent event: UIEvent){
        dismiss(animated: true, completion: nil)
    }

}
