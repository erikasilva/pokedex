//
//  AppDelegate.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 4/12/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure() //Busca el archivo GoogleService-Info.plist y configura la aplicacion.
        IQKeyboardManager.shared.enable = true //hace un scroll cuando el teclado aparece si es necesario
        
        //print("USER MAIL: \(Auth.auth().currentUser?.email)") //Se obtiene el usuario actual que esta loggeado en la app
        /*if let _ = Auth.auth().currentUser {
            let userInfoViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController //En este caso como no se tiene un storyboard, hay que inicializarlo. Y despues si se puede instanciar.
            window?.rootViewController = userInfoViewController //Se le indica que la pantalla principal va a ser la que se acaba de instanciar.
        }
        let db = Firestore.firestore()
        //La diferencia con el if es que lo que se crea en el guard se puede leer afuera del bloque.
        guard let currentUser = Auth.auth().currentUser else{
            return true
        }
        //Obtiene el usuario loggeado, si existe entonces pasa a la pantalla User Info
        //No se va a ejecutar aqui porque es una llamada asincrona. 
        let userRef = db.collection("users").document(currentUser.uid)
        userRef.getDocument{(snapshot, error) in
            if let _ = error{
                let mainViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
                self.window?.rootViewController = mainViewController
            }else{
                let mainViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                self.window?.rootViewController = mainViewController
            }
        }*/
        print("UserDefaults: \(UserDefaults.standard.array(forKey: "favPokemon"))")
        return true
    }
}
