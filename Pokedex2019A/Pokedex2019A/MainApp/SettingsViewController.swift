//
//  SettingsViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 6/14/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func signOutButtonPressed(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        }catch{
            
        }
    }
}
