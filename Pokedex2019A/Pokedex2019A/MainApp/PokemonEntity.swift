//
//  PokemonEntity.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 6/18/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import Foundation
import RealmSwift

class PokemonEntity: Object{
    @objc dynamic var pokemonId: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var height: Double = 0
    @objc dynamic var weight: Double = 0
    @objc dynamic var imageURL: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var isFavourite = false
    
    convenience init(pokemon: Pokemon) { //convenience: permite sobreescribir el constructor.
        self.init()
        pokemonId = pokemon.pokemonId!
        name = pokemon.name!
        height = pokemon.height ?? 0
        weight = pokemon.weight ?? 0
        imageURL = pokemon.imageURL ?? ""
        url = pokemon.url ?? ""
    }
    //Para que los pokemon sean unicos
    override static func primaryKey() -> String? {
        return "pokemonId"
    }
    
}

