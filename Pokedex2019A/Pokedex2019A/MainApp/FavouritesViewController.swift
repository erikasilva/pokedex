//
//  FavouritesViewController.swift
//  Pokedex2019A
//
//  Created by Erika Silva on 6/18/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import RealmSwift

class FavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var favouritesTableView: UITableView!
    var nextURL = "https://pokeapi.co/api/v2/pokemon"
    var favouritesPokemon = [PokemonEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemon()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    func getPokemon(){
        let realm = try! Realm()
        self.favouritesPokemon = Array(realm.objects(PokemonEntity.self).filter("isFavourite = true"))
        self.favouritesTableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouritesPokemon.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        
        cell.pokemonNameLabel?.text = favouritesPokemon[indexPath.row].name.capitalized
        cell.pokemonImageView.kf.setImage(with: URL(string: favouritesPokemon[indexPath.row].imageURL))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            let selectedPokemon = favouritesPokemon[favouritesTableView.indexPathForSelectedRow?.row ?? 0]
            destination.pokemonURL = selectedPokemon.url
            destination.pokemonName = selectedPokemon.name
            destination.isFavouriteScreen = true
        }
    }
}
